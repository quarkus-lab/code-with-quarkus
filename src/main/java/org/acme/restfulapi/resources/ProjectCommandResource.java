package org.acme.restfulapi.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.acme.restfulapi.aop.MyLogger;
import org.acme.restfulapi.common.Common;
import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.*;
import org.acme.restfulapi.services.*;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/projects/v1")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@MyLogger
@Slf4j
public class ProjectCommandResource {

    @Inject
    private Common common;

    @Inject
    private AddProjectService addProjectService;
    @Inject
    private UpdateProjectService updateProjectService;
    @Inject
    private UpdateProjectStatusService updateProjectStatusService;
    @Inject
    private DeleteProjectService deleteProjectService;


    @POST
    public ResponseDto<ProjectRespDto> addProject(AddProjectReqDto request) throws JsonProcessingException {

        common.logRequest(request);

        Project project = addProjectService.addProject(request);
        return addProjectService.buildResponse(project);
    }

    @PUT
    public ResponseDto<ProjectRespDto> updateProject(UpdateProjectReqDto request) {

        Project project = updateProjectService.updateProject(request);
        return updateProjectService.buildResponse(project);
    }

    @PUT
    @Path("/status")
    public ResponseDto<List<ProjectRespDto>> updateProjectStatus(UpdateProjectStatusReqDto request) {

        List<Project> projects = updateProjectStatusService.updateProjectStatus(request);
        return updateProjectStatusService.buildResponse(projects);
    }

    @DELETE
    @Path("/{id}")
    public ResponseDto<List<ProjectRespDto>> deleteProject(@PathParam Integer id) {

        List<Project> projects = deleteProjectService.deleteProject(id);
        return deleteProjectService.buildResponse(projects);
    }
}
