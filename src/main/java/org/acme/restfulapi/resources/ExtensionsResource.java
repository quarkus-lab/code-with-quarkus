package org.acme.restfulapi.resources;

import org.acme.restfulapi.resources.dto.AddProjectReqDto;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.acme.restfulapi.services.ExtensionsService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;

@Path("/extension")
public class ExtensionsResource {

    @Inject
    @RestClient
    private ExtensionsService extensionsService;

    @GET
    @Path("/getAllProjects")
    public ResponseDto<List<ProjectRespDto>> getAllProjects() {
        return extensionsService.getProjects();
    }

    @POST
    @Path(("/newProject"))
    public ResponseDto<ProjectRespDto> newProject(AddProjectReqDto request) {
        return extensionsService.newProject(request);
    }

}
