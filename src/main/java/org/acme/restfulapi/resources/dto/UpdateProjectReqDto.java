package org.acme.restfulapi.resources.dto;

import lombok.Data;

@Data
public class UpdateProjectReqDto {
    private Integer id;
    private String name;
    private String description;
    private String status;
}
