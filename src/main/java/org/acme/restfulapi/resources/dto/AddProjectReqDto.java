package org.acme.restfulapi.resources.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddProjectReqDto {

    private String name;
    private String description;
    private String status;

}
