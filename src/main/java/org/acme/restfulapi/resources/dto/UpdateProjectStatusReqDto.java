package org.acme.restfulapi.resources.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateProjectStatusReqDto {

    private Set<Integer> ids;
    private String status;
}
