package org.acme.restfulapi.resources.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto<T> {

    @JsonProperty("RETURN_CODE")
    private String returnCode;
    @JsonProperty("RETURN_MESSAGE")
    private String returnMessage;
    @JsonProperty("DETAIL")
    private T detail;

    public ResponseDto(String returnCode, String returnMessage) {
        this.returnCode = returnCode;
        this.returnMessage = returnMessage;
    }
}
