package org.acme.restfulapi.resources;

import lombok.extern.slf4j.Slf4j;
import org.acme.restfulapi.aop.MyLogger;
import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.acme.restfulapi.services.GetProjectByNameService;
import org.acme.restfulapi.services.GetProjectService;
import org.acme.restfulapi.services.GetProjectsService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/projects/v1")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@MyLogger
@Slf4j
public class ProjectQueryResource {

    @Inject
    private GetProjectsService getProjectsService;
    @Inject
    private GetProjectService getProjectService;
    @Inject
    private GetProjectByNameService getProjectByNameService;

    @GET
    public ResponseDto<List<ProjectRespDto>> getProjects() {

        List<Project> projects = getProjectsService.getProjects();
        return getProjectsService.buildResponse(projects);
    }

    @GET
    @Path("/{id}")
    public ResponseDto<ProjectRespDto> getProject(@PathParam Integer id) {

        Project project = getProjectService.getProject(id);
        return getProjectService.buildResponse(project);
    }

    @GET
    @Path("/name/{name}")
    public ResponseDto<List<ProjectRespDto>> getProjectByName(@PathParam String name) {

        List<Project> projects = getProjectByNameService.getProjectByName(name);
        return getProjectByNameService.buildResponse(projects);
    }
}
