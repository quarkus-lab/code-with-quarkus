package org.acme.restfulapi.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.WebApplicationException;

@MyLogger
@Interceptor
@Slf4j
public class CustomerInterceptor {

    ObjectMapper objectMapper = new ObjectMapper();

    @AroundInvoke
    Object logInvocation(InvocationContext ctx) throws Exception {

        logRequest(ctx);
        try {
            Object response = ctx.proceed();
            logResponse(response);
            return response;
        } catch (WebApplicationException e) {
            logError(e);
            return e;
        }
    }

    private void logError(WebApplicationException e) {
        log.error("error: {}", e);
    }

    private void logResponse(Object response) throws JsonProcessingException {
        String resp = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);
        log.info("response: {}", resp);
    }

    private void logRequest(InvocationContext ctx) {
    }
}
