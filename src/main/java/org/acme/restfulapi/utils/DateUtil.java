package org.acme.restfulapi.utils;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@ApplicationScoped
public class DateUtil {

    //一般西元日期格式
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withLocale(Locale.TAIWAN)
            .withZone(ZoneId.systemDefault());
    //DB2 timestamp格式
    private DateTimeFormatter timeStampFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withLocale(Locale.TAIWAN)
            .withZone(ZoneId.systemDefault());

    /**
     * 取得DB格式的時間  ex：1911-01-01
     * @return
     */
    public String getDBDate() {
        return dateFormatter.format(LocalDate.now());
    }

    /**
     * 取得DB格式的TimeStamp，非UNIX時間戳記  ex：1911-01-01 00:00:00
     * @return
     */
    public String getDBTimeStamp() {
        return timeStampFormatter.format(LocalDateTime.now());
    }

    public LocalDate getLocalDate() {
        return LocalDate.now();
    }

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.parse(getDBTimeStamp(), timeStampFormatter);
    }
}
