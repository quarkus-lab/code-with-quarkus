package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.AddProjectReqDto;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.acme.restfulapi.utils.DateUtil;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@ApplicationScoped
@Transactional(rollbackOn = Exception.class)
public class AddProjectService {

    @Inject
    private DateUtil dateUtil;


    public Project addProject(AddProjectReqDto request) {

        LocalDateTime now = dateUtil.getLocalDateTime();

        Project project = Project.builder()
                .name(request.getName())
                .description(request.getDescription())
                .status(request.getStatus())
                .createTime(now)
                .updateTime(now)
                .build();
        Project.persist(project);
        return project;
    }

    public ResponseDto<ProjectRespDto> buildResponse(Project project) {
        if (project == null) {
            return new ResponseDto<>("0001", "新增失敗");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            ProjectRespDto detail = modelMapper.map(project, new TypeToken<ProjectRespDto>() {
            }.getType());

            return new ResponseDto<>("0000", "新增成功", detail);
        }
    }
}
