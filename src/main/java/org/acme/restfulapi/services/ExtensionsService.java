package org.acme.restfulapi.services;

import org.acme.restfulapi.resources.dto.AddProjectReqDto;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;

@RegisterRestClient(configKey = "extensions-api")
@Path("/projects/v1")
public interface ExtensionsService {

    @GET
    ResponseDto<List<ProjectRespDto>> getProjects();

    @POST
    ResponseDto<ProjectRespDto> newProject(AddProjectReqDto request);
}
