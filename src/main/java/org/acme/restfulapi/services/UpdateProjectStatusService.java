package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.acme.restfulapi.resources.dto.UpdateProjectStatusReqDto;
import org.acme.restfulapi.utils.DateUtil;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@ApplicationScoped
@Transactional(rollbackOn = Exception.class)
public class UpdateProjectStatusService {

    @Inject
    private DateUtil dateUtil;

    public List<Project> updateProjectStatus(UpdateProjectStatusReqDto updateProjectStatusReqDto) {

        Set<Integer> ids = updateProjectStatusReqDto.getIds();
        String status = updateProjectStatusReqDto.getStatus();
        LocalDateTime now = dateUtil.getLocalDateTime();

        Project.updateProjectStatus(status, now, ids);

        return Project.findProjectsByIds(ids);
    }

    public ResponseDto<List<ProjectRespDto>> buildResponse(List<Project> projects) {
        if (projects.isEmpty()) {
            return new ResponseDto<>("0001", "更新失敗");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            List<ProjectRespDto> detail = modelMapper.map(projects, new TypeToken<List<ProjectRespDto>>() {
            }.getType());

            return new ResponseDto<>("0000", "更新成功", detail);

        }
    }
}
