package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.acme.restfulapi.resources.dto.UpdateProjectReqDto;
import org.acme.restfulapi.utils.DateUtil;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@ApplicationScoped
@Transactional(rollbackOn = Exception.class)
public class UpdateProjectService {

    @Inject
    private DateUtil dateUtil;

    public Project updateProject(UpdateProjectReqDto updateProjectReqDto) {

        Integer id = updateProjectReqDto.getId();
        String name = updateProjectReqDto.getName();
        String description = updateProjectReqDto.getDescription();
        String status = updateProjectReqDto.getStatus();
        LocalDateTime now = dateUtil.getLocalDateTime();

        Optional<Project> optional = Project.findByIdOptional(id);
        Project project = optional.orElse(null);
        if (project == null) {
            return null;
        }
        project.name = name;
        project.description = description;
        project.status = status;
        project.updateTime = now;

        return project;
    }

    public ResponseDto<ProjectRespDto> buildResponse(Project project) {
        if (project == null) {
            return new ResponseDto<>("0001", "更新失敗");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            ProjectRespDto detail = modelMapper.map(project, new TypeToken<ProjectRespDto>() {
            }.getType());

            return new ResponseDto<>("0000", "更新成功", detail);
        }
    }
}
