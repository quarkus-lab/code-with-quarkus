package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@Transactional(rollbackOn = Exception.class)
public class DeleteProjectService {

    public List<Project> deleteProject(Integer id) {

        Optional<Project> optional = Project.findByIdOptional(id);
        Project project = optional.orElse(null);
        if (project == null) {
            return null;
        }
        project.delete();

        return Project.findAll().list();
    }

    public ResponseDto<List<ProjectRespDto>> buildResponse(List<Project> projects) {
        if (projects == null) {
            return new ResponseDto<>("0001", "查無刪除資料");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            List<ProjectRespDto> detail = modelMapper.map(projects, new TypeToken<List<ProjectRespDto>>() {
            }.getType());

            return new ResponseDto<>("0000", "刪除成功", detail);

        }
    }
}
