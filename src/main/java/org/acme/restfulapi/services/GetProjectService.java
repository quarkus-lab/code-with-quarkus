package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GetProjectService {


    public Project getProject(Integer id) {

        return Project.findById(id);
    }

    public ResponseDto<ProjectRespDto> buildResponse(Project project) {
        if (project == null) {
            return new ResponseDto<>("0001", "查無資料");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            ProjectRespDto detail = modelMapper.map(project, new TypeToken<ProjectRespDto>() {
            }.getType());

            return new ResponseDto<>("0000", "查詢成功", detail);
        }
    }
}
