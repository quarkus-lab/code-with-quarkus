package org.acme.restfulapi.services;

import org.acme.restfulapi.model.Project;
import org.acme.restfulapi.resources.dto.ProjectRespDto;
import org.acme.restfulapi.resources.dto.ResponseDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class GetProjectByNameService {

    public List<Project> getProjectByName(String name) {

        return Project.find("name", name).list();
    }

    public ResponseDto<List<ProjectRespDto>> buildResponse(List<Project> projects) {
        if (projects.isEmpty()) {
            return new ResponseDto<>("0001", "查無資料");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            List<ProjectRespDto> detail = modelMapper.map(projects, new TypeToken<List<ProjectRespDto>>() {
            }.getType());

            return new ResponseDto<>("0000", "查詢成功", detail);
        }
    }
}
