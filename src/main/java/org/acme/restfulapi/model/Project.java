package org.acme.restfulapi.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Project")
@Builder
public class Project extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    public String description;
    public String status;
    @Column(name = "create_time")
    public LocalDateTime createTime;
    @Column(name = "update_time")
    public LocalDateTime updateTime;


    public static List<Project> findProjectsByIds(Set<Integer> ids) {
        return Project.find("id in ?1", ids).list();
    }

    public static void updateProjectStatus(String status, LocalDateTime now , Set<Integer> ids) {
        Project.update("status = ?1, update_time = ?2 where id in ?3", status, now, ids);
    }

}
