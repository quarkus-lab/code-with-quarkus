package org.acme.restfulapi.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@Slf4j
public class Common {

    ObjectMapper objectMapper = new ObjectMapper();

    public <T> void logRequest(T request) throws JsonProcessingException {

        String req = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
        log.info("request: {}", req);

    }
}
