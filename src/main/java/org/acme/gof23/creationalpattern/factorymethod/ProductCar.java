package org.acme.gof23.creationalpattern.factorymethod;

import org.jboss.logging.Logger;

public class ProductCar implements Product {

    private static final Logger log = Logger.getLogger(ProductBus.class);

    private String productType = "Car";
    private String productParameter = "A";

    @Override
    public String getProductType() {
        return productType;
    }

    public String getProductParameter(){
        return productParameter;
    }

    @Override
    public void doUse() {
        log.info("You can drive car");
    }
}
