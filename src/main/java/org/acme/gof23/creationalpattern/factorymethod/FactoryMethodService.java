package org.acme.gof23.creationalpattern.factorymethod;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FactoryMethodService {

    @Inject
    FactoryImpl factory;

    public Product buildProduct(String parameter) {
        return factory.buildProduct(parameter);
    }
}
