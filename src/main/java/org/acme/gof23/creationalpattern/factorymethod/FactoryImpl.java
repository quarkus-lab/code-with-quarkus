package org.acme.gof23.creationalpattern.factorymethod;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FactoryImpl implements Factory {
    @Override
    public Product buildProduct(String parameter) {
        if (parameter.equals("A")) {
            return new ProductCar();
        } else if (parameter.equals("B")) {
            return new ProductBus();
        } else {
            return null;
        }
    }
}
