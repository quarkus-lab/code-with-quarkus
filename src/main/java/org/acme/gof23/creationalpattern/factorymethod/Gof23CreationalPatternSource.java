package org.acme.gof23.creationalpattern.factorymethod;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/gof23")
public class Gof23CreationalPatternSource {

    @Inject
    FactoryMethodService factoryMethodService;

    @GET
    @Path("/factoryMethod/{parameter}")
    @Produces(MediaType.APPLICATION_JSON)
    public Product factoryMethod(@PathParam String parameter){
        Product product = factoryMethodService.buildProduct(parameter);
        product.doUse();
        return product;
    }
}
