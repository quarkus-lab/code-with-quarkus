package org.acme.gof23.creationalpattern.factorymethod;

public interface Product {

    String productType = null;
    String getProductType();
    void doUse();
}
