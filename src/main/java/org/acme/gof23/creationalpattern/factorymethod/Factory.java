package org.acme.gof23.creationalpattern.factorymethod;

public interface Factory {

    Product buildProduct(String parameter);
}
